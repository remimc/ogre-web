#*-* coding: utf-8 *-*

from django.http import HttpResponse
from lib.facade import AppFacade

class OrganizationRolesAuthMiddleware(object):
	def process_request(self, request):
		client_addr = request.META.get('REMOTE_ADDR')
		
		if client_addr == '127.0.0.1':
			return
		
		
		app = AppFacade()
		
		response = HttpResponse()
		response.status_code = 403
		
		try:	
			machine = app.find_machine(client_addr, find_by='ip')[0]
			member = app.find_member(machine.owner, find_by='id')[0]
			roles = app.find_roles_for_member(member)
			print(client_addr)
			print(roles)
			if 'ADMIN' not in roles and 'RESEAU' not in roles:
				return response
		except Exception as e:
			return response
