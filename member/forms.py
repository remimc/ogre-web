#-*- coding: utf-8 -*-
from django import forms
from lib.facade import AppFacade
from lib.exceptions import MachineError

# Form for member infos, used for adding new member
class MemberAddForm(forms.Form):
	error_css_class = 'error'
	required_css_class = 'required'
	
	firstname = forms.CharField(label="Prénom")
	lastname = forms.CharField(label="Nom")
	email = forms.EmailField(label="Email",
		error_messages={
			'invalid':"Ceci n'est pas une adresse email valide"
		}
	)
	newsok = forms.BooleanField(label="Recevoir les news", initial=True)
	room = forms.ChoiceField(label="Chambre")

# Form for registration infos, used for adding new member
class MemberRegForm(forms.Form):
	error_css_class = 'error'
	required_css_class = 'required'
	
	registration = forms.ChoiceField(
		label="Durée de l'adhésion",
		widget=forms.RadioSelect,
		required=True,
	)
	duration = forms.DecimalField(
		label="Nombre de mois",
		min_value=1,
		max_value=12,
		decimal_places=0,
		required=False,
	)
	regfrom = forms.DateField(
		widget=forms.DateInput(format=['%Y-%m-%d']),
		label="Date d'inscription",
		input_formats=['%Y-%m-%d'],
		required=True,
	)

# Form for extend a member registration
class MemberExtendForm(forms.Form):
	error_css_class = 'error'
	required_css_class = 'required'
	
	duration = forms.DecimalField(
		label="Nombre de mois",
		min_value=1,
		max_value=12,
		decimal_places=0,
		required=False,
	)

# Form to add a machine, used for adding new members, for adding new machines and for updating
class MachineAddForm(forms.Form):
	error_css_class = 'error'
	required_css_class = 'required'
	
	mac = forms.RegexField(label="Adresse MAC",
		regex=r'([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}',
		error_messages={
			'invalid':"Une adresse MAC doit être de la forme 'xx:xx:xx:xx:xx:xx' (x de '0' à '9' ou de 'a' à 'f')"
		}
	)
	name = forms.CharField(label="Nom de la machine", required=False)
	
	def clean_mac(self):
		app = AppFacade()
		data = self.cleaned_data['mac']
		try:
			app.find_machine(data, find_by='mac')
			raise forms.ValidationError(u"L'addresse mac '%s' existe déja" % data)
		except MachineError as e:
			pass
		return data

# Form used to choose the ordering criteria of the member list 
class MemberOrderForm(forms.Form):
	ORDER_CHOICES = (("firstname", "Prénom"), ("lastname", "Nom"), ("email", "Email"), ("room", "Chambre"))
	order_by = forms.ChoiceField(label="Trier par", choices=ORDER_CHOICES)
