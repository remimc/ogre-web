#*-* coding: utf-8 *-*

from django.conf.urls import patterns, include, url

urlpatterns = patterns('member.views',
    url(r'^$', 'home', name='member_home'),
    
	url(r'^show/(?P<id>.*)$', 'show', name='member_show'),
    
	url(r'^add/$', 'add', name='member_add'),
    
	url(r'^edit/(?P<id>.*)/block$', 'block', name='member_block'),
    
	url(r'^edit/(?P<id>.*)/validate$', 'validate', name='member_validate'),

	url(r'^edit/(?P<id>.*)/news$', 'news', name='member_news'),
    
	url(r'^edit/(?P<id>.*)/machine/add$', 'add_machine', name='member_add_machine'),
    
	url(r'^edit/(?P<id>.*)/machine/(?P<machine>([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2})$', 'edit_machine', name='member_edit_machine'),
    
	url(r'^edit/(?P<id>.*)/machine/(?P<machine>([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2})/delete$', 'del_machine', name='member_del_machine'),
    
	url(r'^edit/(?P<id>.*)/infos$', 'edit_infos', name='member_edit_infos'),
    
	url(r'^edit/(?P<id>.*)/payment/add$', 'add_payment', name='member_add_payment'),
    
	url(r'^edit/(?P<id>.*)/payment/(?P<payment>\d*)$', 'edit_payment', name='member_edit_payment'),
    
	url(r'^edit/(?P<id>.*)/remove$', 'remove', name='member_remove'),
	
	url(r'^edit/(?P<id>.*)/extend$', 'extend', name='member_extend'),
	
	url(r'^edit/(?P<id>.*)/renew$', 'renew', name='member_renew'),
)
