#*-* coding: utf-8 *-*

# Create your views here.
from uuid import uuid4
from datetime import date

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage

from member.forms import MemberAddForm, MemberRegForm, MachineAddForm
from member.forms import MemberOrderForm

from member.utils import is_year_registration
from member.utils import is_last_year

from lib.utilities import month_add, remove_accents
from lib.facade import AppFacade
from lib.entity.member import Member
from lib.entity.machine import Machine
from lib.entity.payment import Payment

def home(request):
	"""
	"""
	
	app = AppFacade()
	
	# Create and init form
	member_order_form = MemberOrderForm(data=request.POST or None)
	
	# Get GET parameters
	order_by = request.GET.get('order_by')
	if 'page' in request.GET:
		page = int(request.GET.get('page'))
	else:
		page = 1
	
	# Check GET request 'order_by' parameter
	if not MemberOrderForm(data={'order_by':order_by}).is_valid():
		order_by = 'firstname'
		
	# Set 'order_by' form default value to the latest selected value (from GET parameters)
	member_order_form.fields['order_by'].initial = order_by
	
	# Handle form data
	if request.method == 'POST':
		# Validate form
		if member_order_form.is_valid():
			order_by = member_order_form.cleaned_data.get('order_by')
			num_pages = member_order_form.cleaned_data.get('num_pages')
	
	# Get all ordered members
	members = app.find_all_members(order_by=order_by)
	
	# Set paginator for current page, last page if errors
	paginator = Paginator(members, 20)
	try:
		member_page = paginator.page(page)
	except EmptyPage:
		member_page = paginator.page(paginator.num_pages)
	
	return render(request, 'member/home.html', {
		'order_form': member_order_form,
		'member_page': member_page,
		'order_by': order_by,
	})

def add(request):
	"""
	"""
	
	app = AppFacade()
	
	# Create and init forms
	member_add_form = MemberAddForm(data=request.POST or None)
	member_reg_form = MemberRegForm(data=request.POST or None, initial={"regfrom": str(date.today())})
	MachineAddFormSet = formset_factory(MachineAddForm, extra=4)	
	machine_add_formset = MachineAddFormSet(data=request.POST or None)
	
	# Make the choices list for the room field from all the free rooms
	# (ordered by buildings)
	rooms = app.find_free_rooms()
	rooms_for_buildings = dict()
	for room in rooms:
		if room.location not in rooms_for_buildings:
			rooms_for_buildings[room.location] = list()
		rooms_for_buildings[room.location].append(room)
	buildings = sorted(rooms_for_buildings.keys())
	member_add_form.fields['room'].choices = tuple([('Batiment ' + str(b), tuple([(r.name, r.name) for r in rooms_for_buildings[b]])) for b in buildings])
	
	# Make the choices list for the duration field (year or month)
	member_reg_form.fields['registration'].choices = (("year", "Adhésion a l'année"), ("month", "Adhésion au mois"))
	
	# Validate the forms
	if request.method == 'POST':	
		if member_add_form.is_valid() and member_reg_form.is_valid() and machine_add_formset.is_valid():
			member = Member()
			payment = Payment()
			
			# Generate member unique id
			member.id = str(uuid4())
			
			# Member Infos
			member.firstname = remove_accents(member_add_form.cleaned_data.get('firstname')).lower()
			member.lastname = remove_accents(member_add_form.cleaned_data.get('lastname')).lower()
			member.email = member_add_form.cleaned_data.get('email')
			member.newsok = member_add_form.cleaned_data.get('newsok')
			member.room = member_add_form.cleaned_data.get('room')
			member.blocked = False
			member.regvalid = True
			
			# Registration & payment Infos
			member.regfrom = member_reg_form.cleaned_data.get('regfrom')
			registration = member_reg_form.cleaned_data.get('registration')
			
			payment.member = member.id
			payment.date = member.regfrom
			
			# Year registration (12 months, until the next 30 september)
			if registration == 'year':
				payment.type = 'ADHESION_ANNEE'
				payment.amount = float(50)
				payment.comment = "1 an"
				
				if member.regfrom.month <= 8:
					# If month before august, end date is the 30 september of the same year
					member.networkuntil = date(member.regfrom.year, 9, 30)
				else:
					# Otherwise, its the 30 september of the next year
					member.networkuntil = date(member.regfrom.year + 1, 9, 30)
				
			# Month registration
			elif registration == 'month':
				payment.type = 'ADHESION_MOIS'
				duration = member_reg_form.cleaned_data.get('duration')
				payment.amount = float(duration * 6)
				payment.comment = "%s mois" % (duration)
				
				# End date corresponds to the paid months
				member.networkuntil = month_add(member.regfrom, duration)
			
			
			messages.info(request, "Membre : " + str(member))
			messages.info(request, "Paiement : " + str(payment))
			
			# Machine Infos
			# Get all valid mac adresses
			machines = list()
			for form in machine_add_formset:
				if form.cleaned_data:
					machine = Machine()
					machine.mac = form.cleaned_data.get('mac').lower()
					machine.name =  form.cleaned_data.get('name')
					if machine.name == "":
						machine.name = None
					machine.owner = member.id
					machines.append(machine)
					messages.info(request, "Machine : " + str(machine))
			
			
			app.add_member(member)
			app.add_payment_to_member(payment, member)
			for machine in machines:
				app.add_machine_to_member(machine, member)
			
			return redirect("member_home")
		else:
			messages.warning(request, "Attention ! Il y a des erreurs")
		
	return render(request, 'member/add.html', {
		'member_add_form': member_add_form,
		'machine_add_formset': machine_add_formset,
		'member_reg_form': member_reg_form
	})
	
def show(request, id):
	"""
	"""
	
	app = AppFacade()
	
	# Create and init the form to add machines
	machine_add_form = MachineAddForm(data=request.POST or None)
	
	# Get all the informations, payments, machines concerning the given member
	member = app.find_member(id, find_by='id')[0]
	payments = app.find_payments_for_member(member)
	machines = app.find_machines_for_member(member)
	
	can_renew = False
	can_extend = False
	
	if is_last_year(member.regfrom):
		# Has registered last academic year, so have to renew
		can_renew = True
	else:
			# Has been registered this year, so if this is a record in years,
			# we can extend subscription
			if not is_year_registration(member.networkuntil):
				can_extend = True
	
	print vars()
	
	return render(request, 'member/show.html', {
		'member': member,
		'payments': payments,
		'machines': machines,
		'machine_add_form': machine_add_form,
		'can_renew': can_renew,
		'can_extend': can_extend,
	})
	
def add_machine(request, id):
	"""
	"""
	
	app = AppFacade()
	
	# Create and init the form to add a new machine
	machine_add_form = MachineAddForm(data=request.POST or None)
	
	# Add a new validated machine
	if request.method == 'POST':
		if machine_add_form.is_valid():
			# Find the member for whom to add machine
			member = app.find_member(id, find_by='id')[0]
			
			# Create machine from form data
			machine = Machine(mac=machine_add_form.cleaned_data['mac'], name=machine_add_form.cleaned_data['name'])
			
			# Add new machine to member
			app.add_machine_to_member(machine, member)
			
			messages.info(request, str(machine))
			messages.info(request, str(member))
			
	return redirect('member_show', id=id)

def edit_machine(request, id, machine):
	"""
	"""
	
	app = AppFacade()
	
	# Create and init the form to edit machines
	machine_add_form = MachineAddForm(data=request.POST or None)
	
	# Find the machine to edit
	machine = app.find_machine(machine, find_by='mac')[0]
	
	# Fill the form fields with initial machine values
	machine_add_form.fields['mac'].initial = machine.mac
	machine_add_form.fields['name'].initial = machine.name
	
	# Update machine with validated data
	if request.method == 'POST':
		if machine_add_form.is_valid():
			# Update the machine
			machine.mac = machine_add_form.cleaned_data['mac']
			machine.name = machine_add_form.cleaned_data['name']
			if machine.name == "":
				machine.name = None
			app.update_machine(machine)
			
			messages.info(request, str(machine))
			
			return redirect('member_show', id=id)
			
	return render(request, 'member/edit_machine.html', {
		'member_id': id,
		'machine': machine,
		'machine_add_form': machine_add_form,
	})

def del_machine(request, id, machine):
	"""
	"""
	
	app = AppFacade()
	
	# Find the machine to delete
	machine = app.find_machine(machine, find_by='mac')[0]
	
	# Delete the machine
	app.remove_machine(machine)
	
	messages.info(request, str(machine))
	
	return redirect('member_show', id=id)

def edit_infos(request, id):
	app = AppFacade()
	member = app.find_member(id, find_by='id')[0]
	
	# Create form and fill fields with initial member values
	member_form = MemberAddForm(request.POST or None)
	member_form.fields['firstname'].initial = member.firstname
	member_form.fields['lastname'].initial = member.lastname
	member_form.fields['email'].initial = member.email
	member_form.fields['newsok'].initial = member.newsok
	
	# Make the choices list for the room field from all the free rooms
	# (ordered by buildings)
	rooms = app.find_free_rooms()
	rooms_for_buildings = dict()
	for room in rooms:
		if room.location not in rooms_for_buildings:
			rooms_for_buildings[room.location] = list()
		rooms_for_buildings[room.location].append(room)
	buildings = sorted(rooms_for_buildings.keys())
	
	# Set up the list of free rooms and default values for the select field (None and current room)
	free_rooms = tuple([('Batiment ' + str(b), tuple([(r.name, r.name) for r in rooms_for_buildings[b]])) for b in buildings])
	if member.room:
		member_form.fields['room'].choices = (("--", "Aucune"), (member.room, member.room)) + free_rooms
	else:
		member_form.fields['room'].choices = (("--", "Aucune"),) + free_rooms
	member_form.fields['room'].initial = member.room
	
	if request.method == 'POST':
		if member_form.is_valid():
			member.firstname = member_form.cleaned_data['firstname']
			member.lastname = member_form.cleaned_data['lastname']
			member.email = member_form.cleaned_data['email']
			member.newsok = member_form.cleaned_data['newsok']
			member.room = member_form.cleaned_data['room']
			if member.room == "--":
				member.room = None
			app.update_member(member)
			
			return redirect('member_show', id=id)
	
	return render(request, 'member/edit_infos.html', {
		'member': member,
		'member_form': member_form,
	})

def add_payment(request, id):
	return HttpResponse()

def edit_payment(request, id, payment):
	return HttpResponse()

def remove(request, id):
	return HttpResponse()

def block(request, id):
	"""
	"""
	
	app = AppFacade()
	
	member = app.find_member(id, find_by='id')[0]
	member.blocked = not member.blocked
	
	app.update_member(member)
	
	return redirect('member_show', id=id)

def validate(request, id):
	"""
	"""
	
	app = AppFacade()
	
	member = app.find_member(id, find_by='id')[0]
	member.regvalid = not member.regvalid
	
	app.update_member(member)
	
	return redirect('member_show', id=id)

def news(request, id):
	"""
	"""
	
	app = AppFacade()
	
	member = app.find_member(id, find_by='id')[0]
	member.newsok = not member.newsok
	
	app.update_member(member)
	
	return redirect('member_show', id=id)

def extend(request, id):
	"""
	"""
	
	app = AppFacade()
	
	member = app.find_member(id, find_by='id')[0]
	
	if request.method == 'POST':
		
		return redirect('member_show', id=id)
	
	return HttpResponse()

def renew(request, id):
	"""
	"""
	
	app = AppFacade()
	
	member = app.find_member(id, find_by='id')[0]
	
	member_renew = MemberRegForm(data=request.POST or None, initial={"regfrom": str(date.today())})
	member_renew.fields['registration'].choices = (("year", "Adhésion a l'année"), ("month", "Adhésion au mois"))
	
	if request.method == 'POST':
		if member_renew.is_valid():
			payment = Payment()
			
			# Registration & payment Infos
			member.regfrom = member_renew.cleaned_data.get('regfrom')
			registration = member_renew.cleaned_data.get('registration')
			
			payment.member = member.id
			payment.date = member.regfrom
			
			# Year registration (12 months, until the next 30 september)
			if registration == 'year':
				payment.type = 'ADHESION_ANNEE'
				payment.amount = float(50)
				payment.comment = "1 an"
				
				if member.regfrom.month <= 8:
					# If month before august, end date is the 30 september of the same year
					member.networkuntil = date(member.regfrom.year, 9, 30)
				else:
					# Otherwise, its the 30 september of the next year
					member.networkuntil = date(member.regfrom.year + 1, 9, 30)
				
			# Month registration
			elif registration == 'month':
				payment.type = 'ADHESION_MOIS'
				duration = member_reg_form.cleaned_data.get('duration')
				payment.amount = float(duration * 6)
				payment.comment = "%s mois" % (duration)
				
				# End date corresponds to the paid months
				member.networkuntil = month_add(member.regfrom, duration)
			
			member.regvalid = True
			app.update_member(member)
			app.add_payment_to_member(payment, member)
			
			return redirect('member_show', id=id)
	
	return render(request, 'member/renew.html', {
		'member': member,
		'member_renew': member_renew
	})