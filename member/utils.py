#*-* coding: utf-8 *-*

from datetime import date

def is_last_year(d, today=date.today()):
	if date(today.year, 9,1) < today < date(today.year, 12, 31):
			return date(today.year - 1, 9, 1) < d < date(today.year, 8, 31)
	elif date(today.year, 1, 1) < today < date(today.year, 8, 31):
			return date(today.year - 2, 9, 1) < d < date(today.year - 1, 8, 31)

def is_year_registration(d, today=date.today()):
	if date(today.year, 9,1) < today < date(today.year, 12, 31):
			return d > date(today.year + 1, 9, 1)
	elif date(today.year, 1, 1) < today < date(today.year, 8, 31):
			return d > date(today.year, 9, 1)

