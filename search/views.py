#*-* coding: utf-8 *-*

# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render
from search.forms import SearchForm
from lib.facade import AppFacade

def home(request):
	app = AppFacade()
	members = list()
	
	# Create and init forms
	form = SearchForm(data=request.POST or None)
	form.fields['by'].choices = (('firstname', 'Prénom'),('lastname', 'Nom'),('email', 'Email'),('room', 'Chambre'))
	
	# Process post request
	if request.method == 'POST':
		# Validate form and process data if cleaned
		if form.is_valid():
			members = app.search_member(form.cleaned_data['search'], search_by=form.cleaned_data['by'])
	
	return render(request, 'search/home.html', {
		'form':form,
		'members':members
	})