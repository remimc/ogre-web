#*-* coding: utf-8 *-*

from django.conf.urls import patterns, include, url

urlpatterns = patterns('search.views',
    url(r'^$', 'home', name='search_home'),
)
